// Use as
// import "wild/db"
// ...
// db := newDb('postgres://user:pass@host:port/database')
// var data [][]string
// rows, err := db.Fetch(`users`, `1`, `2`, `3`)
// if err != nil {
//   return nil
// }
// data, err = db.ToVars(rows, 'name', 'email', 'token')
//
//for complicate queries
//q = `my complicate SQL...`
//rows. err := db.Database.Query(q, val1, val2)
//defer rows.Close()
//for rows.Next() {
//if err := rows.Scan(&value1, &value2); err != nil {
//return nil, err
//}
//do something with fetched values
//}

package db

import (
	"database/sql"
	"strings"

	"errors"

	"time"

	"github.com/lib/pq"
)

type DB struct {
	Database *sql.DB
}

func NewDb(dsn string) (*DB, error) {
	println(`Registered DB drivers:`, sql.Drivers())
	sql.Register(`pq`, &pq.Driver{})
	println(`Registered DB drivers:`, sql.Drivers())
	db, err := sql.Open(`pq`, dsn)
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		for i := 0; i < 10; i++ {
			time.Sleep(time.Second * 5)
			println(`try to connect to db...`)
			err = db.Ping()
			if err == nil {
				break
			}
		}
		if err != nil {
			return nil, err
		}
	}
	println(`connected!`)
	return &DB{
		Database: db,
	}, nil
}

// db.FetchUser(`123456`) => SELECT * FROM users WHERE token IN (123456)
func (db *DB) FetchUser(token string) ([]string, error) {
	println(`db: FetchUser`, token)
	rows, err := db.Fetch(`users`, `token`, token)
	if err != nil {
		return nil, err
	}
	if rows == nil {
		return nil, errors.New(`Empty result fetched!`)
	}
	var (
		id, name, email, hash, geo_lon, geo_lat interface{}
	)
	res, err := db.ToVars(rows, id, name, email, hash, geo_lon, geo_lat)

	//var ret [][]string
	////defer rows.Close()
	//for rows.Next() {
	//	if err := rows.Scan(&id, &name, &email, &hash, &geo_lon, &geo_lat); err != nil {
	//		return nil, err
	//	}
	//	ret = append(ret, []string{id, name, email, hash, geo_lon, geo_lat})
	//}
	//if err := rows.Err(); err != nil {
	//	return nil, err
	//}

	if err != nil {
		return nil, err
	}
	return res[0], err
}

// db.Fetch(`users`, `id`, `1`, `2`, `3`) => SELECT * FROM users WHERE id IN(1, 2, 3)
func (db *DB) Fetch(entity string, field string, id ...string) (*sql.Rows, error) {
	defer func() {
		if err := recover(); err != nil {
			println(err)
		}
	}()
	var q, psubst string
	//subst = strings.Repeat(`?,`, len(id))
	//subst = strings.TrimSuffix(subst, `,`)
	psubst = `'` + strings.Join(id, `','`) + `'`
	q = `SELECT * FROM ` + entity + ` WHERE ` + field + ` IN (` + psubst + `)`
	println(q, psubst)
	rows, err := db.Database.Query(q)
	return rows, err
}

// db.Insert(`users`, `name, email, token, hash`, `Vasya`, `vasya@example.com`, `123456`, `67890`)
// => INSERT INTO users (name, email, token, hash) VALUES (123456, 67890) ON CONFLICT DO NOTHING
func (db *DB) Insert(entity string, colnames string, values ...string) (*sql.Result, error) {
	var q, subst string
	subst = strings.Repeat(`?,`, len(values))
	subst = strings.TrimSuffix(subst, `,`)
	q = `INSERT INTO ` + entity + `(` + colnames + `) VALUES (` + subst + `)` + ` ON CONFLICT DO NOTHING`
	res, err := db.Database.Exec(q, strings.Join(values, `,`))
	return &res, err
}

// db.Update(`users`, `token=123456`, 1) => UPDATE users set TOKEN=123456 WHERE ID IN(1)
func (db *DB) Update(entity string, expr string, id ...string) (*sql.Result, error) {
	var q string
	q = `UPDATE ` + entity + ` SET ` + expr + ` WHERE id IN (?)`
	res, err := db.Database.Exec(q, strings.Join(id, `,`))
	return &res, err
}

// db.Delete(`users`, 1, 2, 3) => DELETE FROM users WHERE ID IN(1,2,3)
func (db *DB) Delete(entity string, id ...string) (*sql.Result, error) {
	var q string
	q = `DELETE FROM ` + entity + `WHERE id IN (?)`
	res, err := db.Database.Exec(q, strings.Join(id, `,`))
	return &res, err
}

func (db *DB) ToVars(rows *sql.Rows, fields ...interface{}) ([][]string, error) {
	var ret [][]string
	//defer rows.Close()
	if err := rows.Err(); err != nil {
		return nil, err
	}
	for rows.Next() {
		if err := rows.Scan(fields...); err != nil {
			return nil, err
		}
		rt := make([]string, len(fields))
		for _, f := range fields {
			if sf, ok := f.(string); ok {
				rt = append(rt, string(sf))
			}
		}
		ret = append(ret, rt)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return ret, nil
}
