package db

import (
	"testing"
	"os"
)

func TestNewDbIntegration(t *testing.T) {
	if os.Getenv(`WILD_INTEGRATION`) != `` {
		_, err := NewDb(`postgres://user:pass@postgres:1234/test`)
		if err != nil {
			t.Error(err)
		}
	}
}
