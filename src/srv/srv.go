package main

import (
	"context"
	"db"
	"encoding/json"
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
)

type Conf struct {
	Port string
	Dsn  string
}

var (
	conf       Conf
	SrvHandler *http.ServeMux
	Server     *http.Server
	Db         *db.DB
	err        error
	userCache  map[string][]string
	exitCh     chan os.Signal
)

func init() {
	flag.StringVar(&conf.Port, `port`, `80`, `port to listen`)
	flag.StringVar(&conf.Dsn, `dsn`, `postgres://wild:crazypass@postgres/wild?sslmode=disable`, `database DSN strind`)
	log.Printf("path: '%s'", &conf.Port)
}
func main() {
	flag.Parse()

	defer func() {
		println(`deferred closing...`)
		Server.Close()
	}()

	println(`accepted config`, conf.Dsn, conf.Port)

	exitCh = make(chan os.Signal, 1)

	//go func() {
	//	<-exitCh
	//	println(`killed...`)
	//	Server.Shutdown(context.Background())
	//}()

	signal.Notify(exitCh, os.Interrupt, os.Kill)

	userCache = make(map[string][]string, 0)

	SrvHandler = http.NewServeMux()

	Db, err = db.NewDb(conf.Dsn)
	if err != nil {
		println(`DB connection error:`, err.Error())
	}

	SrvHandler.HandleFunc(`/api/point/`, getPointInfo)
	SrvHandler.HandleFunc(`/api/stick/`, setPointInfo)
	SrvHandler.HandleFunc(`/api/trace/`, getTrace)
	SrvHandler.HandleFunc(`/api/private/`, getUserOwnInfo)
	SrvHandler.HandleFunc(`/api/user/`, getUserInfo)
	SrvHandler.HandleFunc(`/api/register/`, setUserInfo)
	SrvHandler.HandleFunc(`/api/auth/`, userAuthorize)
	SrvHandler.HandleFunc(`/api/recover/`, userPassRecovery)
	Server = &http.Server{
		Addr:    `:` + conf.Port,
		Handler: SrvHandler,
	}
	println(`listen at`, Server.Addr)
	Server.ListenAndServe()

	<-exitCh
	println(`killed...`)
	Server.Shutdown(context.Background())

}

func getPointInfo(res http.ResponseWriter, req *http.Request) {
	println(`getPointInfo`)
	if fail := denyUnauthorized(&res, req); fail {
		return
	}
	rows, err := Db.Fetch(`points`, `id`)
	denyOnError(err, &res, req)
	fields := []interface{}{`name`, `geo_lon`, `geo_lat`, `description`}
	data, err := Db.ToVars(rows, fields...)
	denyOnError(err, &res, req)
	res.WriteHeader(200)
	res.Write([]byte(makeJson(fields, data)))

}

func setPointInfo(res http.ResponseWriter, req *http.Request) {
	println(`setPointInfo`)
	if fail := denyUnauthorized(&res, req); fail {
		return
	}
}

func getTrace(res http.ResponseWriter, req *http.Request) {
	println(`getTrace`)
	if fail := denyUnauthorized(&res, req); fail {
		return
	}
}

func getUserInfo(res http.ResponseWriter, req *http.Request) {
	println(`getUserInfo`)
	if fail := denyUnauthorized(&res, req); fail {
		return
	}
}

func getUserOwnInfo(res http.ResponseWriter, req *http.Request) {
	println(`getUserInfo`)
	if fail := denyUnauthorized(&res, req); fail {
		return
	}
}

func setUserInfo(res http.ResponseWriter, req *http.Request) {
	println(`setUserInfo`)
	type regInfo struct {
		email    string
		name     string
		password string
	}
	var d regInfo
	println(req.Body)
	json.NewDecoder(req.Body).Decode(d)
	println(`received`, d.email, d.name, d.password)
}

func userAuthorize(res http.ResponseWriter, req *http.Request) {
	println(`userAuthorize`)
	key := req.Header.Get(`accesskey`)
	println(`accesskey`, key)
	if key == `` {
		res.WriteHeader(500)
		res.Write([]byte(`Access key missing`))
		return
	}

	userInfo, err := Db.FetchUser(key)
	if fail := denyOnError(err, &res, req); fail {
		return
	}
	userCache[key] = userInfo
	res.WriteHeader(200)
	res.Write(makeJson([]interface{}{`hash`}, [][]string{[]string{makeHash(key, userInfo[3])}}))

}

func userPassRecovery(res http.ResponseWriter, req *http.Request) {
	println(`userPassRecovery`)
	if fail := denyUnauthorized(&res, req); fail {
		return
	}
}

func denyUnauthorized(res *http.ResponseWriter, req *http.Request) bool {
	println(`denyUnauthorized`)
	if !authIsValid(req.Header.Get(`accesskey`), req.Header.Get(`hash`)) {
		(*res).Header().Set(`location`, `/api/auth/`)
		(*res).WriteHeader(300)
		(*res).Write([]byte(`Access denied, please log in`))
		return true
	}
	return false
}

func denyOnError(err error, res *http.ResponseWriter, req *http.Request) (ret bool) {
	ret = false
	println(`denyOnError`, err.Error(), req.URL.String())
	if err != nil {
		println(`denyOnError: error accepted!`)
		(*res).WriteHeader(500)
		(*res).Write([]byte(`{"error"":"SrvHandler error:}`))
		ret = true
	}
	return
}

func authIsValid(key, hash string) bool {
	println(`authIsValid`)
	userInfo, ok := userCache[key]
	if !ok {
		return false
	}
	return fetchHash(key, hash) == userInfo[3]
}

func makeHash(key, hash string) string {
	return key + hash
}

func fetchHash(key, hash string) string {
	return string([]byte(hash)[len(key):])
}

func makeJson(keys []interface{}, vals [][]string) []byte {
	var (
		res  []byte
		l, m int
	)
	l = len(keys) - 1
	m = len(vals) - 1
	if m > 0 {
		res = append(res, []byte(`[`)...)
	}
	for i, ss := range vals {
		res = append(res, []byte(`{`)...)
		for j, k := range keys {
			res = append(res, []byte(`"`+k.(string)+`":"`+ss[j]+`"`)...)
			if j < l {
				res = append(res, []byte(`,`)...)
			}
		}
		res = append(res, []byte(`}`)...)
		if i < m {
			res = append(res, []byte(`,`)...)
		}
	}
	if m > 0 {
		res = append(res, []byte(`]`)...)
	}
	return res
}

func parseJson(data []byte) map[string]string {
	var res map[string]string

	return res
}

func respondError(msg string, code int) {

}
