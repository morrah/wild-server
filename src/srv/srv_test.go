package main

import (
	"bytes"
	"testing"
	"os"
)

func TestMakeJson(t *testing.T) {
	var (
		keys     []interface{}
		vals     [][]string
		exp, res []byte
	)
	keys = []interface{}{interface{}(`a`), interface{}(`b`)}
	vals = [][]string{
		{`1`, `2`},
		{`3`, `4`},
	}
	exp = []byte(`[{"a":"1","b":"2"},{"a":"3","b":"4"}]`)
	res = makeJson(keys, vals)
	if !bytes.Equal(res, exp) {
		if os.Getenv(`WILD_TEST_VERBOSE_OUTPUT`) != `` {
			println(string(res))
			println(string(exp))
		}
		t.Error(`unexpected makeJson result`)
	}
}
