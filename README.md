# wild-server core server part of IMWILD service

basic API server based on Go standard library with minimum dependencies
and main data storage on postgresql

dependencies managed by [https://github.com/rancher/trash/]

to avoid using huge jvm-based task managers like Gradle or Ant simplest `deploy.sh` presents

## installation
- git clone https://bitbucket.org/imwild/wild-server.git wild
- cd wild

to install dependencies
- go get github.com/rancher/trash
- cd <your_installation_path>/wild
- export GOPATH=$PWD
- ~go/bin/trash -k -d . -T src/vendor -f vendor.conf (path to your `trash` dependencies manager may differ)

to update dependencies
- ~go/bin/trash -u -k -d . -T src/vendor -f vendor.conf


## compilation
- ./deploy.sh clean build

## run stand-alone API server
- ./bin/srv --dsn=`your DB dsn srting` --port=`port to listen`

## build docker images for server and storage
- deploy.sh docker

## run with docker
- deploy.sh run

## create database and tables in db storage
- deploy.sh migrate

## stop docker containers
- deploy.sh stop

## remove docker containers
- deploy.sh rm

## remove docker images
- deploy.sh rmi

# deployment use cases
short deploy.sh commands referense
- deploy.sh help

recompile and build docker images
- deploy.sh clean build docker

run with docker
- deploy.sh run

stop and remove running server and storage
- deploy.sh stop rm rmi

Storage data directory is volumized, so, data are persistent between each deployment actions

To remove data storage permanently
- docker volume rm pgdata

#testing

## automatic testing

used standard `go test` package

most unit tests included to the same packages

- go test `package name`

to run only benchmark tests
- go test `package name` -benchmem -bench -run __bench

to tun all unit tests
- deploy.sh test

to tun all integration tests
- deploy.sh test-it

## manual testing

API testing commands may be executed from any path

for stand-alone running server `IP` is your local machine IP or `localhost`

for server running in docker container `IP` is one assigned to container by docker (see `docker network` command)

- curl -H 'content-type: application/json' -d '{"email":"mail@example.com","name":"user","password":"123456"}' '`IP`:`port`/api/register/'

- curl -H 'content-type: application/json' -H 'accesstoken: 123456' '`IP`:`port`/api/auth/'

- curl -H 'content-type: application/json' -d '{"email":"mail@example.com"}' '`IP`:`port`/api/recover/'

- curl -H 'content-type: application/json' -H 'accesstoken: 123456' '`IP`:`port`/api/private/'

- curl -H 'content-type: application/json' -H 'accesstoken: 123456' -d '{"id":"123456"}' '`IP`:`port`/api/user/'

- curl -H 'content-type: application/json' -H 'accesstoken: 123456' -d '{"geo_lon":"105.678","geo_lat":"85.678","name":"My favorite place","description":"The best place to be all alone"}' '`IP`:`port`/api/stick/'

- curl -H 'content-type: application/json' -H 'accesstoken: 123456' -d '{"id":"123456"}' '`IP`:`port`/api/point/'

- curl -H 'content-type: application/json' -H 'accesstoken: 123456' -d '{"id":"123456"}' '`IP`:`port`/api/trace/'
