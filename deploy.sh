#!/bin/sh
whatican () {
echo 'Specify the command: 
- build to build binaries
- clean to remove binaries
- docker to build docker image with binaries
- test to run unit tests
- test-it to run integration tests
- run to run docke container
- stop to stop docker container
- rm to remove stopped docker container
- rmi to remove docker image with binaries
'
}
echo "$@\n"
if [ -z "$1" ]; then
whatican
fi

for arg in "$@"
do
case $arg in
build)
GOPATH=$PWD go build -o ./bin/srv ./src/srv
ls -lsa ./bin
echo 'Binaries are built!'
;;
clean)
rm ./bin/*
ls -lsa ./bin
echo 'Binaries are removed'
;;
docker)
docker build -f Dockerfile -t wild --label wild .
echo `docker images | grep wild`
echo 'docker image is built!'
;;
test)
GOPATH=$PWD go test db srv
;;
test-it)
GOPATH=$PWD WILD_INTEGRATION='1' go test db srv
;;
run)
echo 'Checking network...'
check=`docker network ls | grep wild`
echo $check
if [ -z `docker network ls | grep wild` ]; then
docker network create -d bridge --label wild wild
echo `docker network ls | grep wild`
echo 'network created!'
else
echo 'network exists...'
fi
docker run -d --name wild --network wild --network-alias wildsrv  wild /bin/sh -c '/bin/wild/srv --port=8080'
docker run -d --name postgres --volume pgdata:/var/lib/postgresql/data --volume $PWD/db_migrations:/migrations --network wild --network-alias postgres postgres:9.6
echo `docker ps -a --no-trunc | grep wild`
echo `docker ps -a --no-trunc | grep postgres`
echo 'docker containers started!'
;;
stop)
docker stop wild
docker stop postgres
echo `docker ps -a --no-trunc | grep wild`
echo `docker ps -a --no-trunc | grep postgres`
echo 'docker containers stopped!'
;;
migrate)
docker exec postgres /bin/sh -c 'psql -U postgres -E -f /migrations/creation.sql && psql -U postgres -d wild -E -f /migrations/migration.sql'
#docker exec postgres psql -U postgres -E -c $`cat ./migration.sql`
echo `docker ps -a --no-trunc | grep postgres`
;;
rm)
docker rm wild
echo `docker ps -a --no-trunc | grep wild`
echo 'docker container removed!'
;;
rmi)
docker rmi `docker images -q --filter "label=wild"`
echo `docker images | grep wild`
echo 'docker image is removed!'
;;
help)
whatican
;;
*)

;;
esac
done

