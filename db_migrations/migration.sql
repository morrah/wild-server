CREATE TABLE IF NOT EXISTS points (
id integer UNIQUE PRIMARY KEY,
geo_lon FLOAT,
geo_lat FLOAT,
name VARCHAR(255),
description TEXT,
area_id integer
);

CREATE TABLE IF NOT EXISTS areas (
id integer UNIQUE PRIMARY KEY,
geo_lon FLOAT,
geo_lat FLOAT,
name VARCHAR(255),
description TEXT
);

CREATE TABLE IF NOT EXISTS users (
id integer UNIQUE PRIMARY KEY,
hash VARCHAR(255),
token VARCHAR(255),
geo_lon FLOAT,
geo_lat FLOAT,
name VARCHAR(255),
status TEXT
);

CREATE TABLE IF NOT EXISTS users_points (
id integer UNIQUE PRIMARY KEY,
user_id integer,
point_id integer,
name VARCHAR(255),
description TEXT
);

CREATE TABLE IF NOT EXISTS users_profiles (
id integer UNIQUE PRIMARY KEY,
user_id integer,
email VARCHAR(255),
description TEXT
);
